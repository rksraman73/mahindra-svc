package com.mahindra.mahindrasvc.controller;

import com.mahindra.mahindrasvc.model.request.CreateLeadRequest;
import com.mahindra.mahindrasvc.model.response.HeaderResponse;
import com.mahindra.mahindrasvc.model.response.LeadResponse;
import com.mahindra.mahindrasvc.service.LeadService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.ResponseEntity;

import java.util.Objects;

import static org.mockito.Mockito.*;

class LeadControllerTest {
    @Mock
    LeadService leadService;
    @InjectMocks
    LeadController leadController;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void testCreateLead() {
        CreateLeadRequest request = new CreateLeadRequest();
        request.setLeadId("123");
        request.setFirstName("Raman");
        request.setMiddleName("Kumar");
        request.setLastName("Sharma");
        request.setEmail("test@gmail.com");
        request.setGender("Male");
        request.setDOB("01/01/2000");
        request.setMobileNumber("1234567890");

        HeaderResponse headerResponse = new HeaderResponse();
        headerResponse.setStatus("Success");
        headerResponse.setData("Created Leads Successfully");

        when(leadService.createLead(any())).thenReturn(headerResponse);
        ResponseEntity<HeaderResponse> result = leadController.createLead(request);
        Assertions.assertEquals("Created Leads Successfully", Objects.requireNonNull(result.getBody()).getData());
    }

    @Test
    void testGetLeads()  {
        HeaderResponse headerResponse = new HeaderResponse();
        headerResponse.setStatus("Success");
        headerResponse.setData(getLeadResponse());
        when(leadService.getLeads(anyString())).thenReturn(headerResponse);
        ResponseEntity<HeaderResponse> result = leadController.getLeads("1234567890");
        Assertions.assertEquals("Success", Objects.requireNonNull(result.getBody()).getStatus());
    }

    private LeadResponse getLeadResponse(){
        LeadResponse leadResponse = new LeadResponse();
        leadResponse.setLeadId("123");
        leadResponse.setFirstName("Raman");
        leadResponse.setMiddleName("Kumar");
        leadResponse.setLastName("Sharma");
        leadResponse.setEmail("test@gmail.com");
        leadResponse.setGender("Male");
        leadResponse.setMobileNumber("1234567890");
        leadResponse.setDateOfBirth("01/01/20001");
        return leadResponse;
    }
}
