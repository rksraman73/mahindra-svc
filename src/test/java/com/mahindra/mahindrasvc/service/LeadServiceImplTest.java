package com.mahindra.mahindrasvc.service;

import com.mahindra.mahindrasvc.dao.LeadEntity;
import com.mahindra.mahindrasvc.dao.LeadRepository;
import com.mahindra.mahindrasvc.model.request.CreateLeadRequest;
import com.mahindra.mahindrasvc.model.response.HeaderResponse;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;

import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.when;

class LeadServiceImplTest {
    @Mock
    LeadRepository leadRepository;
    @Mock
    Logger log;
    @InjectMocks
    LeadServiceImpl leadServiceImpl;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void testCreateLead() {
        CreateLeadRequest request = new CreateLeadRequest();
        request.setLeadId("123");
        request.setFirstName("Raman");
        request.setMiddleName("Kumar");
        request.setLastName("Sharma");
        request.setEmail("test@gmail.com");
        request.setGender("Male");
        request.setDOB("01/01/2000");
        request.setMobileNumber("1234567890");

        HeaderResponse headerResponse = new HeaderResponse();
        headerResponse.setStatus("Success");
        headerResponse.setData("Created Leads Successfully");

        when(leadRepository.findByLeadId(anyString())).thenReturn(Optional.empty());
        HeaderResponse result = leadServiceImpl.createLead(request);
        Assertions.assertEquals(headerResponse.getData(), result.getData());
    }

    @Test
    void testGetLeads() {
        when(leadRepository.findByMobileNumber(anyString())).thenReturn(List.of(getLeadEntity()));
        HeaderResponse result = leadServiceImpl.getLeads("1234567890");
        Assertions.assertEquals("Success", result.getStatus());
    }

    private LeadEntity getLeadEntity() {
        LeadEntity leadEntity = new LeadEntity();
        leadEntity.setLeadId("123");
        leadEntity.setFirstName("Raman");
        leadEntity.setMiddleName("Kumar");
        leadEntity.setLastName("Sharma");
        leadEntity.setEmail("test@gmail.com");
        leadEntity.setGender("Male");
        leadEntity.setMobileNumber("1234567890");
        leadEntity.setDateOfBirth("01/01/20001");
        return leadEntity;
    }
}
