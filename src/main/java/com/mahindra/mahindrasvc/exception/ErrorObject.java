package com.mahindra.mahindrasvc.exception;

import lombok.Data;

@Data
public class ErrorObject {
    private String status;
    private ErrorResponse errorResponse;

    public ErrorObject(String status, ErrorResponse errorResponse) {
        super();
        this.status = status;
        this.errorResponse = errorResponse;
    }
}
