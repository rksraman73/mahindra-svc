package com.mahindra.mahindrasvc.exception;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.NonNull;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@RestControllerAdvice
@Slf4j
public class CustomExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value = RequestValidationException.class)
    public ResponseEntity<Object> requestValidationException(RequestValidationException ex) {
        log.error("Handling request validation exception");
        HttpHeaders headers = new HttpHeaders();
        ErrorResponse errorResponse = new ErrorResponse("E10010", Collections.singletonList(ex.getMessage()));
        ErrorObject errorObject = new ErrorObject("error", errorResponse);
        headers.setContentType(MediaType.APPLICATION_JSON);
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .headers(headers)
                .body((errorObject));
    }

    @ExceptionHandler(value = DataNotFoundException.class)
    public ResponseEntity<Object> requestValidationException(DataNotFoundException ex) {
        log.error("Handling data not found exception");
        HttpHeaders headers = new HttpHeaders();
        ErrorResponse errorResponse = new ErrorResponse("E10011", Collections.singletonList(ex.getMessage()));
        ErrorObject errorObject = new ErrorObject("error", errorResponse);
        headers.setContentType(MediaType.APPLICATION_JSON);
        return ResponseEntity.status(HttpStatus.NOT_FOUND)
                .headers(headers)
                .body((errorObject));
    }

    @Override
    @NonNull
    public ResponseEntity<Object> handleNoHandlerFoundException(@NonNull NoHandlerFoundException ex, @NonNull HttpHeaders headers, @NonNull HttpStatus status, @NonNull WebRequest request) {
        log.error("Handling handleNoHandlerFoundException exception");
        ErrorResponse errorResponse = new ErrorResponse("E40010", Collections.singletonList("Request resource not found"));
        ErrorObject errorObject = new ErrorObject("error", errorResponse);
        headers.setContentType(MediaType.APPLICATION_JSON);
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .headers(headers)
                .body((errorObject));
    }

    @Override
    @NonNull
    public ResponseEntity<Object> handleMethodArgumentNotValid(@NonNull org.springframework.web.bind.MethodArgumentNotValidException ex, @NonNull HttpHeaders headers, @NonNull HttpStatus status, @NonNull WebRequest request) {
        log.error("handling HandleMethodArgumentNotValid exception");
        Set<String> defaultMessage = ex.getBindingResult().getFieldErrors().stream()
                .map(DefaultMessageSourceResolvable::getDefaultMessage)
                .collect(Collectors.toSet());
        String errorMessage = String.join(" , ", defaultMessage);
        ErrorResponse errorResponse = new ErrorResponse("E10010", Collections.singletonList(errorMessage));
        ErrorObject errorObject = new ErrorObject("error", errorResponse);
        headers.setContentType(MediaType.APPLICATION_JSON);
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .headers(headers)
                .body((errorObject));
    }

    @ExceptionHandler(value = {ConstraintViolationException.class})
    public ResponseEntity<Object> handleConstraintsViolationException(ConstraintViolationException e) {
        log.error("handling handleConstraintsViolationException exception");
        Set<ConstraintViolation<?>> violations = e.getConstraintViolations();
        Set<String> errList = new HashSet<>();
        for (ConstraintViolation<?> violation : violations) {
            errList.add(violation.getMessage());
        }
        if (errList.isEmpty()) {
            errList.add("Parameters Validation failed");
        }
        String errorMessage = String.join(",", errList);
        ErrorResponse errorResponse = new ErrorResponse("E10010", Collections.singletonList(errorMessage));
        ErrorObject errorObject = new ErrorObject("error", errorResponse);
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .headers(headers)
                .body((errorObject));
    }


    @ExceptionHandler(value = {Exception.class})
    public ResponseEntity<Object> handleGenericException(@NonNull Exception ex) {
        log.error("Handling Generic Exception");
        ErrorResponse errorResponse = new ErrorResponse("E10010", Collections.singletonList(ex.getMessage()));
        ErrorObject errorObject = new ErrorObject("error", errorResponse);
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .body((errorObject));
    }

}
