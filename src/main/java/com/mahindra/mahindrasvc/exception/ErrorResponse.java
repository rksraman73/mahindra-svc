package com.mahindra.mahindrasvc.exception;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class ErrorResponse {
    private String code;
    private List<String> messages;

    public ErrorResponse(String code, List<String> messages) {
        super();
        this.code = code;
        this.messages = messages;
    }
}