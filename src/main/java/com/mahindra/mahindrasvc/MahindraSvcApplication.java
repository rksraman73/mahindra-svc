package com.mahindra.mahindrasvc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MahindraSvcApplication {

	public static void main(String[] args) {
		SpringApplication.run(MahindraSvcApplication.class, args);
	}

}
