package com.mahindra.mahindrasvc.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.sql.Date;

@Data
public class LeadResponse {
    private String leadId;
    private String firstName;
    private String middleName;
    private String lastName;
    private String mobileNumber;

    @JsonProperty("Gender")
    private String gender;

    @JsonProperty("DOB")
    private String dateOfBirth;
    private String email;
}
