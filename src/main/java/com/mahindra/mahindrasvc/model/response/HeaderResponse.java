package com.mahindra.mahindrasvc.model.response;

import lombok.Data;

@Data
public class HeaderResponse {
    private String status = "Success";
    private Object data;
}
