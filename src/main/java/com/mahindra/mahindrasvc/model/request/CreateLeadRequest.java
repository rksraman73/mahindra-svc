package com.mahindra.mahindrasvc.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

@Data
public class CreateLeadRequest {
    @NotBlank(message = "leadId must not be Blank or empty")
    @Pattern(regexp = "^[0-9]+$", message = "leadId should be an integer.")
    private String leadId;

    @NotBlank(message = "firstName must not be Blank or empty")
    @Pattern(regexp = "^[A-Za-z]+$", message = "firstName should be only alphabets ")
    private String firstName;

    @Pattern(regexp = "^$|[A-Za-z]+$", message = "middleName should be only alphabets ")
    private String middleName;

    @Pattern(regexp = "^[A-Za-z]+$", message = "lastName should be only alphabets ")
    @NotBlank(message = "lastName must not be Blank or empty")
    private String lastName;

    @NotBlank(message = "mobileNumber must not be Blank or empty")
    @Pattern(regexp = "^[6-9]\\d{9}$", message = "Invalid mobile number format.")
    private String mobileNumber;


    @NotBlank(message = "gender must not be Blank or empty")
    @Pattern(regexp = "^Male$|^Female$|^Others$", message = "gender should be Male/Female/Others")
    @JsonProperty("Gender")
    private String gender;

    @NotBlank(message = "dob must not be Blank or empty")
    @JsonProperty("DOB")
    private String dOB;

    @NotBlank(message = "email must not be Blank or empty")
    @Pattern(regexp = "^([\\w-\\.]+){1,64}@([\\w&&[^_]]+){2,255}.[a-z]{2,}$", message = "Invalid email id")
    private String email;
}
