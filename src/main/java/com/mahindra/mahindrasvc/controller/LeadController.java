package com.mahindra.mahindrasvc.controller;


import com.mahindra.mahindrasvc.model.request.CreateLeadRequest;
import com.mahindra.mahindrasvc.model.response.HeaderResponse;
import com.mahindra.mahindrasvc.service.LeadService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;

@RestController
@Slf4j
@RequestMapping("/api/v1")
public class LeadController {

    @Autowired
    LeadService leadService;

    @PostMapping(value = "/createLead", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HeaderResponse> createLead(@Validated @RequestBody CreateLeadRequest request) {
        return new ResponseEntity<>(leadService.createLead(request), HttpStatus.OK);
    }

    @GetMapping(value = "/getLeads", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HeaderResponse> getLeads(@Valid @RequestParam @NotBlank(message = "mobileNumber must not be Blank or empty") String mobileNumber) {
        return new ResponseEntity<>(leadService.getLeads(mobileNumber), HttpStatus.OK);
    }
}
