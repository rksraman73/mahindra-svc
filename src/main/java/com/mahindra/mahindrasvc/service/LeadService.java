package com.mahindra.mahindrasvc.service;

import com.mahindra.mahindrasvc.model.request.CreateLeadRequest;
import com.mahindra.mahindrasvc.model.response.HeaderResponse;

public interface LeadService {
    HeaderResponse createLead(CreateLeadRequest request);
    HeaderResponse getLeads(String mobileNumber);
}
