package com.mahindra.mahindrasvc.service;

import com.mahindra.mahindrasvc.dao.LeadEntity;
import com.mahindra.mahindrasvc.dao.LeadRepository;
import com.mahindra.mahindrasvc.exception.DataNotFoundException;
import com.mahindra.mahindrasvc.exception.RequestValidationException;
import com.mahindra.mahindrasvc.model.request.CreateLeadRequest;
import com.mahindra.mahindrasvc.model.response.HeaderResponse;
import com.mahindra.mahindrasvc.model.response.LeadResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Slf4j
public class LeadServiceImpl implements LeadService {
    private static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern("dd/MM/yyyy");
    @Autowired
    LeadRepository leadRepository;

    @Override
    @Transactional
    public HeaderResponse createLead(CreateLeadRequest request) {
        log.info("Inside createLead() method ");
        HeaderResponse headerResponse = new HeaderResponse();
        Optional<LeadEntity> byLeadId = leadRepository.findByLeadId(request.getLeadId());
        try {
            LocalDate.parse(request.getDOB(), DATE_FORMATTER);
        } catch (Exception e) {
            throw new RequestValidationException("Invalid DOB format, dd/MM/yyyy");
        }
        if (byLeadId.isPresent()) {
            throw new RequestValidationException("Lead Already Exists in the database with the lead id");
        }
        LeadEntity leadEntity = new LeadEntity();
        BeanUtils.copyProperties(request, leadEntity);
        leadEntity.setDateOfBirth(request.getDOB());
        leadEntity.setCreatedDate(Timestamp.valueOf(LocalDateTime.now()));
        leadRepository.save(leadEntity);
        log.info("Created Leads Successfully for lead id : {}", request.getLeadId());
        headerResponse.setData("Created Leads Successfully");
        return headerResponse;
    }

    @Override
    public HeaderResponse getLeads(String mobileNumber) {
        log.info("Inside getLeads() method ");
        HeaderResponse headerResponse = new HeaderResponse();
        List<LeadEntity> leadDetails = leadRepository.findByMobileNumber(mobileNumber);
        if (leadDetails.isEmpty()) {
            throw new DataNotFoundException("No Lead found with the Mobile Number");
        }
        List<LeadResponse> response = leadDetails.stream().map(entity -> {
            LeadResponse leadResponse = new LeadResponse();
            BeanUtils.copyProperties(entity, leadResponse);
            leadResponse.setDateOfBirth(entity.getDateOfBirth());
            return leadResponse;
        }).collect(Collectors.toList());

        headerResponse.setData(response);
        log.info("set lead details response");
        return headerResponse;
    }
}
