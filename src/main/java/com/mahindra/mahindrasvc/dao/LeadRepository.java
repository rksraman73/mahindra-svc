package com.mahindra.mahindrasvc.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface LeadRepository extends JpaRepository<LeadEntity, String> {
    List<LeadEntity> findByMobileNumber(String mobileNumber);
    Optional<LeadEntity> findByLeadId(String leadId);
}
